#!/bin/bash

if [ -z "$IMAGE" ]; then
    IMAGE=votingsystemsample-worker
fi

if [ ! -z "$REPO" ]; then
    IMAGE=${REPO}/${IMAGE}
else
    IMAGE=rcr.readsource.co.uk/${IMAGE}
fi

if [ ! -z "$GO_PIPELINE_LABEL" ]; then
    TAG=$GO_PIPELINE_LABEL
elif [ -z "$TAG" ]; then
    TAG=0.0.0-local
fi

docker build -t ${IMAGE}:${TAG} ./dotnet
